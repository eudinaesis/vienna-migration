% Migration: Vienna
% Peter Northup - Hertha Firnberg Schulen
% 11.1.2018

# Definitions and concepts

## Definitions
Ausländer/-innen
  ~ Anyone without Austrian citizenship (15.3%)

In Ausland geborene Bevölkerung
  ~ Born outside Austria (18.9%); 31% have AT citizenship

Bevölkerung mit Migrationshintergrund
  ~ Statistik Austria: both parents born abroad (22%)
  ~ Alternatively: *either* no Austrian citizenship *or* born abroad
  ~ Vienna MA 17 (Integration & Diversity): at least *one* parent born abroad